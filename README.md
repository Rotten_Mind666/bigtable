# Google Bigtable Python Script

This script is designed to interact with Google Bigtable using Python. It includes functionalities to create, read, and delete tables in a specified Bigtable instance. Additionally, it provides methods to interact with data within these tables using CSV files.

## Prerequisites

- Python 3.x
- Google Cloud SDK
- pandas library (`pip install pandas`)

## Installation

1. Install Python 3.x from [python.org](https://www.python.org/downloads/).
2. Install Google Cloud SDK from [cloud.google.com/sdk](https://cloud.google.com/sdk).
3. Install pandas library using pip: `pip install pandas`.

## Configuration

Before running the script, make sure you have configured your Google Cloud credentials. You can set up authentication by running:

```bash
gcloud auth login

Usage
Command-line Arguments
The script accepts the following command-line arguments:

--project_id: Google Cloud project ID. Default is "geospendingtest".
--instance_id: Google Cloud Bigtable instance ID. Default is "geospending".
--table: Name of the Bigtable table to interact with. Default is "MCTD_ITA_overall".
--csv: Name of the CSV file to read data from. Default is "MCTD_ITA_overall".
Running the Script
To run the script, execute it from the command line:
python bigtable_script.py [--project_id PROJECT_ID] [--instance_id INSTANCE_ID] [--table TABLE] [--csv CSV]

Functionality
1. Create Table
The createTable function creates a new table in the specified Bigtable instance. It reads data from a CSV file and populates the table with the CSV data.

2. Read Table
The readTable function retrieves data from the specified table in the Bigtable instance. It can fetch a single row or scan for all rows in the table.

3. Delete Table
The deleteTable function deletes the specified table from the Bigtable instance.

Example
python bigtable_script.py --project_id my_project --instance_id my_instance --table my_table --csv my_data

This command will delete the existing table named "my_table" in the Google Cloud project "my_project" and instance "my_instance", then create a new table with data from the CSV file "my_data.csv".

Feel free to modify and expand upon this README to better suit your project's needs.