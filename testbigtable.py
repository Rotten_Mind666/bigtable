#!/usr/bin/env python

import argparse
import datetime
import pandas as pd
import sys
import os

from google.cloud import bigtable
from google.cloud.bigtable import column_family
from google.cloud.bigtable import row_filters


def connect(project_id, instance_id):
    client = bigtable.Client(project=project_id, admin=True)
    instance = client.instance(instance_id)
    return instance
    
def createTable(table_id, csv, project_id, instance_id):    
    instance = connect(project_id, instance_id)
    prefixstoragecsv = 'testBigTable'
    csv_path = os.path.join(prefixstoragecsv, csv + ".csv")
    if not os.path.exists(csv_path):
        sys.exit('Errore: file CSV non trovato. Assicurati di aver inserito il nome corretto senza specificare l\'estensione') 
    
    csv_data = pd.read_csv(csv_path, sep=';')
    columnsList = csv_data.columns

    print("Creazione della tabella {}.".format(table_id))
    table = instance.table(table_id)
    if not table.exists():
        print("Created table {}.".format(table_id))
        table.create()
       
    print("Creazione delle colonne con regola di Garbage Collection Max Version")
    max_versions_rule = column_family.MaxVersionsGCRule(2)
    column_families = table.list_column_families()

    rows = [] 
    for col, value in csv_data.items():        
        if col in list(column_families.keys()):
            print('Colonna gia\' esistente: '+col)
        else:
            print('Colonna creata: '+col)
            newColumn = table.column_family(col, max_versions_rule).create()
        for i, v in enumerate(value):
            row_key = i.encode('UTF-8')
            row = table.row(row_key)
            row.set_cell(col.encode(), col.encode(), str(v).encode('UTF-8'), timestamp=datetime.datetime.utcnow())
            rows.append(row)
    table.mutate_rows(rows)
        
    print("Scrittura dati in corso...")   

def readTable(table_id, selectedRow, project_id, instance_id): 
    instance = connect(project_id, instance_id)
    row_filter = row_filters.CellsColumnLimitFilter(1)
    print("Getting a single greeting by row key.")
    key = "greeting0".encode()
    row = table.read_row(key, row_filter)
    cell = row.cells[column_family_id][column][0]
    print(cell.value.decode("utf-8"))
    print("Scanning for all greetings:")
    partial_rows = table.read_rows(filter_=row_filter)

    for row in partial_rows:
        cell = row.cells[column_family_id][column][0]
        print(cell.value.decode("utf-8"))

def deleteTable(table_id, project_id, instance_id):
    instance = connect(project_id, instance_id)
    table = instance.table(table_id)
    print("Deleting the {} table.".format(table_id))
    table.delete()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--project_id", default="project_id")
    parser.add_argument("--instance_id", default="instance_id")
    parser.add_argument("--table", default="table")
    parser.add_argument("--csv", default="csv")
    args = parser.parse_args()

    deleteTable(args.table, args.project_id, args.instance_id)
    createTable(args.table, args.csv, args.project_id, args.instance_id)